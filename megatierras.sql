-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2018 at 06:23 am
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `megatierras`
--

-- --------------------------------------------------------

--
-- Table structure for table `generales`
--

CREATE TABLE IF NOT EXISTS `generales` (
  `id` int(11) NOT NULL,
  `correo` varchar(500) NOT NULL,
  `telefono` varchar(500) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `facebook` varchar(500) NOT NULL,
  `instagram` varchar(500) NOT NULL,
  `link_direccion` varchar(500) NOT NULL,
  `imagen` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `generales`
--

INSERT INTO `generales` (`id`, `correo`, `telefono`, `direccion`, `facebook`, `instagram`, `link_direccion`, `imagen`) VALUES
(1, 'info@megatierras.com', '+507 388 4498/99', 'Av Panamericana, sector Las Mañitas, Ofibodegas Panamá, Galera 16, Ciudad de Panamá', 'https://www.facebook.com/inversionesmegatierras/', 'https://www.instagram.com/invmegatierraspa/', 'https://www.google.co.ve/maps/place/Las+Ma%C3%B1anitas/@9.0932087,-79.415637,15z/data=!4m8!1m2!2m1!1ssector+Las+Ma%C3%B1itas,+Ciudad+de+Panam%C3%A1!3m4!1s0x8fab54df79b9f2c7:0x4d3ffbac0f492c3e!8m2!3d9.0799059!4d-79.3997155', 'upload/generales/2018_04_12_01.51.21banner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `nosotros`
--

CREATE TABLE IF NOT EXISTS `nosotros` (
  `id` int(11) NOT NULL,
  `historia` text NOT NULL,
  `mision` text NOT NULL,
  `vision` text NOT NULL,
  `imagen` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nosotros`
--

INSERT INTO `nosotros` (`id`, `historia`, `mision`, `vision`, `imagen`) VALUES
(1, '<p class="text">\r\nLa empresa se inició en el año 2001 dedicándose, principalmente a la construcción de obras civiles como estructuras y proyectos llave en mano.\r\n</p>\r\n<p class="text">\r\nHa participado interrumpidamente en el desarrollo Regional EN VENEZUELA para el sector privado básicamente en el NOR-ORIENTE del país, construyendo obras que son orgullo para el Edo. Anzoátegui y para la empresa a lo largo y ancho de la ciudad.\r\n</p>\r\n<p class="text">\r\nPara finales del 2014 llega a la REPÚBLICA DE PANAMÁ afianzándose con su gente y comunidades para desempeñar la misma labor, pero también dedicándose a la importación de productos y materiales de acabado para la construcción, como proyectos industriales y residenciales, y así poder satisfacer las necesidades de nuestros actuales y futuros clientes.\r\n</p>', '<p class="text">\r\nSer reconocida como la empresa constructora líder en el país, a través de generación de proyectos, negocios y soluciones integrales para nuestros clientes.\r\n</p>\r\n<p class="text">\r\nAsí mismo ser líderes en importación y venta de materiales para acabados en el proceso de la construcción como revestimiento de paredes, pisos y equipamiento de baños, de forma de ser competitivos y mantenernos a la vanguardia en diseños ofreciendo siempre calidad y buen servicio.\r\n</p>', '<p class="text">Agregar valores a los servicios entregados y crecer como empresa con cada nuevo proyecto que aprendamos.</p>\r\n\r\n<p class="text">\r\nMantener como nuestra prioridad la calidad técnica, la innovación tecnológica y constructiva, las relaciones de confianza con nuestros clientes y fomentar un clima laboral que estimule el desarrollo personal y profesional de nuestros trabajadores.</p>', 'upload/nosotros/2018_04_12_06.06.48nosotros.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `objetivo`
--

CREATE TABLE IF NOT EXISTS `objetivo` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `texto` text NOT NULL,
  `imagen` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objetivo`
--

INSERT INTO `objetivo` (`id`, `titulo`, `texto`, `imagen`) VALUES
(1, 'Lograr La Satisfacción', 'de nuestros clientes en el menor plazo de ejecución convirtiendo sus necesidades en soluciones tangibles, asi como también brindar caldiad y servicio en la venta de materiales para acabado', 'upload/objetivo/2018_04_12_05.28.13banner2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `proyectos`
--

CREATE TABLE IF NOT EXISTS `proyectos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `texto` text NOT NULL,
  `imagen1` text,
  `imagen2` text,
  `fecha` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proyectos`
--

INSERT INTO `proyectos` (`id`, `titulo`, `texto`, `imagen1`, `imagen2`, `fecha`) VALUES
(1, 'Santa María Golf And Country Club- Panamá', 'Armado de Viga y losa postensada en casa 83 Santa María Golf and Country Club Ciudad de Panamá', 'upload/proyectos/2018_04_12_03.49.19proyecto1.jpg', 'upload/proyectos/2018_04_12_03.49.19-2proyecto1-1.png', '2010-02-02'),
(2, 'Santa María Golf And Country Club- Panamá', 'Construcción de Estructura en Casa 06 Santa María Golf and Country Club Ciudad de Panamá', 'upload/proyectos/2018_04_12_04.18.16proyecto2.png', 'upload/proyectos/2018_04_12_04.18.16-2proyecto2-1.png', '2010-02-02'),
(3, 'Santa María Golf And Country Club- Panamá', 'Acabados de Cielo Razo en Gypsum con diseños Arquitectónicos. Santa María Golf and Country Club Ciudad de Panamá', 'upload/proyectos/2018_04_12_04.21.34proyecto3.png', 'upload/proyectos/2018_04_12_04.21.34-2proyecto3-1.png', '2010-02-02'),
(4, 'Las Anclas Mall. Chorrera - Panamá', 'Nivelación de Terreno, Construcción de Fundaciones para Losa Postensada, Losas Postensadas. Las Anclas Mall. Carretera Panamericana, Chorrera. Panamá', 'upload/proyectos/2018_04_12_04.23.03proyecto4.png', 'upload/proyectos/2018_04_12_04.23.03-2proyecto4-1.png', '2010-02-02'),
(5, 'Los Pueblos-Panamá', 'Chorreado  de Concreto y Piso Pulido con Helicóptero en Galeras. Sector Juan Díaz, Los Pueblos, Ciudad de Panamá', 'upload/proyectos/2018_04_12_04.24.32proyecto5.png', 'upload/proyectos/2018_04_12_04.24.32-2proyecto5-1.png', '2010-02-02'),
(6, 'Sector Milla 8-Panamá', 'Encofrado y Vaciado de Muro de Concreto Armado. Avenida Transístmica, Sector Milla 8 Ciudad de Panamá', 'upload/proyectos/2018_04_12_04.26.20proyecto6.png', 'upload/proyectos/2018_04_12_04.26.20-2proyecto6-1.png', '2010-02-02'),
(7, 'Parque Industrial Las Americas, Galeras Whare House. Panamá.', 'Construcción de Fundaciones, Losa y Acabados en Galeras Whare House, ubicadas dentro del Parque Industrial las Américas. Panamá.', 'upload/proyectos/2018_04_12_04.27.50proyecto7.png', 'upload/proyectos/2018_04_12_04.27.50-2proyecto7-1.png', '2010-02-02'),
(8, 'Plaza El Trébol 2, Sector Pacora. Panamá.', 'Construcción de Plaza El Trébol 2, ubicada en Sector Pacora Panamá.', 'upload/proyectos/2018_04_12_04.29.29proyecto8.png', 'upload/proyectos/2018_04_12_04.29.29-2proyecto8-1.png', '2010-02-02'),
(9, 'Maquinarias, Equipos y Vehículos', 'Retroexcavadora, Telehandler, Camiónes Plataforma', 'upload/proyectos/2018_04_12_04.32.07proyecto9.png', 'upload/proyectos/2018_04_12_04.32.07-2proyecto9-1.png', '2010-02-02');

-- --------------------------------------------------------

--
-- Table structure for table `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicios`
--

INSERT INTO `servicios` (`id`, `titulo`, `imagen`) VALUES
(1, 'LEVANTAMIENTO TOPOGRÁFICO', 'upload/servicios/2018_04_12_03.06.23icono2.png'),
(2, 'DISEÑO DE PROYECTO PLANOS Y PERISOLOGÍA', 'upload/servicios/2018_04_12_03.09.38icono3.png'),
(3, 'MOVIMIENTO DE TIERRA  SOLICITAR', 'upload/servicios/2018_04_12_03.10.24icono4.png'),
(4, 'PILOTAJE HINCADO Y ROTATIVO', 'upload/servicios/2018_04_12_03.11.31icono5.png'),
(5, 'GERENCIA DE PROYECTO PLANIFICACIÓN Y EJECUCIÓN', 'upload/servicios/2018_04_12_03.12.35icono6.png'),
(6, 'CONSTRUCCIÓN CONSTRUIMOS TU PROYECTO CON LLAVE EN MANO', 'upload/servicios/2018_04_12_03.13.20icono7.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `generales`
--
ALTER TABLE `generales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nosotros`
--
ALTER TABLE `nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objetivo`
--
ALTER TABLE `objetivo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `generales`
--
ALTER TABLE `generales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nosotros`
--
ALTER TABLE `nosotros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `objetivo`
--
ALTER TABLE `objetivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
