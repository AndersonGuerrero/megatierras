<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class IndexController extends AppController
{

    public function index(){
      $this->generales = (new Generales())->find_first();
      $this->servicios = (new Servicios())->find('order: id');
      $this->proyectos = (new Proyectos())->find('order: id');
      $this->objetivo = (new Objetivo())->find_first();
      $this->nosotros = (new Nosotros())->find_first();
      if (isset($_POST['fullname']) && isset($_POST['email']) &&
          isset($_POST['phone']) && isset($_POST['message']) &&
          filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
             if($_POST['g-recaptcha-response'] != ''){
                 $headers = 'From: ' . $_POST["fullname"] . '<' . $_POST["email"] . '>' . "\r\n" .
                 'Reply-To: ' . $_POST["email"] . "\r\n" .
                 'X-Mailer: PHP/' . phpversion();

                 $message = sprintf( "Nombre: %s\nIntereses: %s\nEmail: %s\nTelefono: %s\n\n Mensaje: %s", $_POST['fullname'], $_POST['intereses'], $_POST['email'], $_POST['phone'], $_POST['message'] );
                 $success = mail( "inversionesmegatierras1221@gmail.com", "Mensaje de Contacto", $message, $headers);

                 if ($success) {
                   Flash::valid("Mensaje enviado satisfactoriamente");
                 }else{
                   Flash::warning("Error, por favor intente nuevamente");
                 }
             }else{
               Flash::warning("Debe marcar el catcha");
             }
     }
    }
}
