<?php
class AdminController extends AppController{

  public function logout(){
    Auth::destroy_identity();
    return Redirect::to('admin/login');
  }

  public function objetivo(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if(Input::hasPost('objetivo')){
      $this->objetivo = (new Objetivo(Input::post('objetivo')));
      $path = getcwd()."/img/upload/objetivo/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $this->objetivo->imagen = "upload/objetivo/".$_FILES["imagen"]["name"];
              }else{
              Flash::warning('No se ha podido subir la imagen ...!!!');
             }
           }
       }
      if($this->objetivo->update()){
        Flash::valid("Actualizado!");
      }
    }else{
      $this->objetivo = (new Objetivo())->find_first();
    }
  }

  public function nosotros(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if(Input::hasPost('nosotros')){
      $this->nosotros = (new Nosotros(Input::post('nosotros')));
      $path = getcwd()."/img/upload/nosotros/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $this->nosotros->imagen = "upload/nosotros/".$_FILES["imagen"]["name"];
              }else{
              Flash::warning('No se ha podido subir la imagen ...!!!');
             }
           }
       }
      if($this->nosotros->update()){
        Flash::valid("Actualizado!");
      }
    }else{
      $this->nosotros = (new Nosotros())->find_first();
    }
  }

  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if(Input::hasPost('generales')){
      $this->generales = (new Generales(Input::post('generales')));
      $path = getcwd()."/img/upload/generales/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $this->generales->imagen = "upload/generales/".$_FILES["imagen"]["name"];
              }else{
              Flash::warning('No se ha podido subir la imagen ...!!!');
             }
           }
       }
      if($this->generales->update()){
        Flash::valid("Actualizado!");
      }
    }else{
      $this->generales = (new Generales())->find_first();
    }
  }

  public function servicios(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->servicios = (new Servicios())->find();
  }
  public function agregar_servicio(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if (Input::haspost("servicios")) {
      $servicios = new Servicios(Input::post("servicios"));
      $path = getcwd()."/img/upload/servicios/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $servicios->imagen = "upload/servicios/".$_FILES["imagen"]["name"];
              }else{
                Flash::warning('No se ha podido subir la imagen ...!!!');
              }
           }
       }
      if ($servicios->save()) {
        Flash::valid("Registrado");
        return Redirect::to('admin/servicios/');
      }else{
        Flash::error("Error al registrar el servicio");
      }
    }
  }
  public function eliminar_servicio($id){
    if (Auth::is_valid()){
       $servicios = new Servicios();
       $servicios = $servicios->find_by_id($id);

       if ($servicios->delete((int)$id)) {
           Flash::valid('Operación exitosa');
       }else{
           Flash::error('Falló Operación');
       }
       return Redirect::to('admin/servicios/');
    }else{
       Flash::valid("Necesita un usuario autenticado");
       return Redirect::to('admin/');
     }
  }

  public function editar_servicio($id) {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if (Input::haspost("servicios")) {
      $servicios = new Servicios(Input::post("servicios"));
      $path = getcwd()."/img/upload/servicios/";

      if(!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
              if ($images->isUploaded()) {
                  if ($images->save()){
                      $servicios->imagen = "upload/servicios/".$_FILES["imagen"]["name"];
                  }else{
                  Flash::warning('No se ha podido subir la imagen ...!!!');
               }
             }
      }

      if ($servicios->update()) {
        Flash::valid("Datos actualizados");
        return Redirect::to('admin/servicios/');
      }else{
        Flash::error("Error al editar, verifique e intente de nuevo");
        return Redirect::to('admin/servicios/editar/'.$id.'/');
      }

    }else {
      $servicios = new Servicios();
      $this->servicios = $servicios->find_by_id((int)$id);
    }
  }

  public function login(){
    if(Auth::is_valid()){
      return Redirect::to('admin');
    }
    View::template('login');
    if(Input::hasPost('users')){
      $pwd = Input::post('users.password');
      $username = Input::post('users.username');
      $auth = new Auth("model", "class: users", "username: $username", "password: $pwd");
      if ($auth->authenticate()) {
        return Redirect::to("admin");
      }else{
        Flash::error('Usuario o contraseña incorrecta!');
      }
    }
  }

  public function agregar_proyectos(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    if (Input::haspost("proyectos")) {
      $proyectos = new Proyectos(Input::post("proyectos"));
      $path = getcwd()."/img/upload/proyectos/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $proyectos->imagen1 = "upload/proyectos/".$_FILES["imagen"]["name"];
              }else{
                Flash::warning('No se ha podido subir la imagen ...!!!');
              }
           }
       }
       if (!empty($_FILES['imagen2']['name'])) {
           $images = Upload::factory('imagen2', 'image');
           $_FILES["imagen2"]["name"] = date("Y_m_d_h.i.s").'-2'.$_FILES['imagen2']['name'];
           $images->setPath($path);
           $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
           if ($images->isUploaded()) {
               if ($images->save()){
                   $proyectos->imagen2 = "upload/proyectos/".$_FILES["imagen2"]["name"];
               }else{
                 Flash::warning('No se ha podido subir la imagen ...!!!');
               }
            }
        }
      if ($proyectos->save()) {
        Flash::valid("Registrado");
        return Redirect::to('admin/proyectos/');
      }else{
        Flash::error("Error al registrar el proyecto");
      }
    }
  }

  public function editar_proyectos($id){
    View::template("admin");
    if (Auth::is_valid()){
      if (Input::haspost("proyectos")) {
        $proyectos = new Proyectos(Input::post("proyectos"));
        $path = getcwd()."/img/upload/proyectos/";

        if(!empty($_FILES['imagen']['name'])) {
            $images = Upload::factory('imagen', 'image');
            $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
            $images->setPath($path);
            $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
                if ($images->isUploaded()) {
                    if ($images->save()){
                        $proyectos->imagen1 = "upload/proyectos/".$_FILES["imagen"]["name"];
                    }else{
                    Flash::warning('No se ha podido subir la imagen ...!!!');
                 }
               }
        }

        if(!empty($_FILES['imagen2']['name'])) {
            $images = Upload::factory('imagen2', 'image');
            $_FILES["imagen2"]["name"] = date("Y_m_d_h.i.s").'2'.$_FILES['imagen2']['name'];
            $images->setPath($path);
            $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
                if ($images->isUploaded()) {
                    if ($images->save()){
                        $proyectos->imagen2 = "upload/proyectos/".$_FILES["imagen2"]["name"];
                    }else{
                    Flash::warning('No se ha podido subir la imagen ...!!!');
                 }
               }
        }

        if ($proyectos->update()) {
          Flash::valid("Datos actualizados");
          return Redirect::to('admin/proyectos/');
        }else{
          Flash::error("Error al editar, verifique e intente de nuevo");
          return Redirect::to('admin/proyectos/editar/'.$id.'/');
        }
      }else {
        $proyectos = new Proyectos();
        $this->proyectos = $proyectos->find_by_id((int)$id);
      }
    }else{
      Flash::valid("Necesita un usuario autenticado");
      return Redirect::to('admin/login');
    }
  }

  public function proyectos(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->proyectos = (new Proyectos())->find();
  }

  public function eliminar_proyecto($id){
    if (Auth::is_valid()){
       $proyectos = new Proyectos();
       $proyectos = $proyectos->find_by_id($id);

       if ($proyectos->delete((int)$id)) {
           Flash::valid('Operación exitosa');
       }else{
           Flash::error('Falló Operación');
       }
       return Redirect::to('admin/proyectos/');
    }else{
       Flash::valid("Necesita un usuario autenticado");
       return Redirect::to('admin/login');
     }
  }
}
